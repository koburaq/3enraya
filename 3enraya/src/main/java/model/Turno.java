package model;

public class Turno {
    private Ficha ficha;

    public Turno(Ficha ficha){
        this.ficha = ficha;
    }

    public void cambiarTurno(){
        if (this.ficha.equals(Ficha.X))
            this.ficha = Ficha.O;
        else
            this.ficha = Ficha.X;
    }

    public Ficha getFicha() {
        return ficha;
    }

    @Override
    public String toString() {
        return this.ficha.toString();
    }
}
